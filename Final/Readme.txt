﻿# LAB 01 - Create SRS document for EcoBikeRental
1.	Nguyễn Văn Trung Hiếu	20173107	Use case Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Use case Tìm kiếm bãi xe, Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Use case Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Use case Xem danh sách bãi xe, Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Use case Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Use case Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Use case Quản lý xe

# LAB 02 - Create Class Analysis Diagram, Complete Architecture Analysis
1.	Nguyễn Văn Trung Hiếu	20173107	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Tìm kiếm bãi xe, Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Xem danh sách bãi xe, Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Biểu đồ trình tự, biểu đồ lớp phân tích cho usecase Quản lý xe

# LAB 03 - Design Interface
1.	Nguyễn Văn Trung Hiếu	20173107	Đặc tả màn hình cho usecase Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Đặc tả màn hình cho usecase Tìm kiếm bãi xe Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Đặc tả màn hình cho usecase Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Đặc tả màn hình cho usecase Xem danh sách bãi xe Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Đặc tả màn hình cho usecase Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Đặc tả màn hình cho usecase Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Đặc tả màn hình cho usecase Quản lý xe
  
# LAB 04 - Design Class
1.	Nguyễn Văn Trung Hiếu	20173107	Biểu đồ trình tự, biểu đồ lớp thiết kế cho usecase Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Biểu đồ trình tự, biểu đồ lớp thiết kế cho usecase Tìm kiếm bãi xe, Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Biểu đồ trình tự, biểu đồ lớp thiết kế cho use case Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Biểu đồ trình tự, biểu đồ lớp thiết kế cho use case Xem danh sách bãi xe, Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Biểu đồ trình tự, biểu đồ lớp thiết kế cho use case Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Biểu đồ trình tự, biểu đồ lớp thiết kế cho use case Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Biểu đồ trình tự, biểu đồ lớp thiết kế cho usecase Quản lý xe

# LAB 05 - Design Database, Complete Detail Design
1.	Nguyễn Văn Trung Hiếu	20173107	Kiểm thử các module phần Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Kiểm thử các module phần Tìm kiếm bãi xe, Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Kiểm thử các module phần Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Kiểm thử các module phần Xem danh sách bãi xe, Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Kiểm thử các module phần Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Kiểm thử các module phần Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Kiểm thử các module phần Quản lý xe
  
# LAB 06 - Unit Test
## Tasks
  - Task 01: Create unit test for usecase rent bike
  - Task 02: Create unit test for usecase return bike
  - Task 03: Create unit test for usecase view station information
  - Task 04: Create unit test for usecase view bike information

  
# LAB 07 - Programming
1.	Nguyễn Văn Trung Hiếu	20173107	Code phần Quản lý thông tin bãi xe
2.	Phạm Lê Minh Trí	20173411	Code phần Tìm kiếm bãi xe, Xem thông tin chi tiết bãi xe
3.	Đinh Quang Đạt	        20173003	Code phần Tìm bãi trả xe
4.	Nguyễn Đức Giang	20173079	Code phần Xem danh sách bãi xe, Xem thông tin chi tiết từng xe
5.	Hoàng Trung Đức	        20173027	Code phần Nhập mã thẻ tín dụng để thanh toán
6.	Nguyễn Thị Nga	        20173278	Code phần Nhập mã để thuê xe
7.	Nguyễn Công Tiệp	20173403	Code phần Quản lý xe
      
