Phân công usecase:
1. Xem danh sách bãi xe				Nguyễn Đức Giang	20173079
2. Xem chi tiết bãi xe				Phạm Lê Minh Trí	20173411
3. Xem chi tiết từng xe				Nguyễn Đức Giang	20173079
4. Nhập mã để thuê xe				Nguyễn Thị Nga		20173278
5. Chọn bãi xe để trả xe 			Đinh Quang Đạt		20173003
6. Tìm kiếm bãi xe 				Phạm Lê Minh Trí	20173411
7. Quản lý thông tin bãi xe			Nguyễn Văn Trung Hiếu	20173107
8. Quản lý thông tin các xe đang sử dụng	Nguyễn Công Tiệp	20173403
9. Nhập mã thẻ tín dụng để thanh toán		Hoàng Trung Đức		20173027